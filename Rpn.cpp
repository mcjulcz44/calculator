#include "Rpn.h"
#include <iostream>
#include <queue>
#include <stack>
#include <string>

using namespace std;

Rpn::Rpn ( string operation )
{
	oper = operation;
}
void Rpn::convert () {
	queue<string> entry;
	stack<string> entry_operators;
	string number;
	for ( int i=0; i < oper.length (); i++ ) {
		string symbol ( 1, oper.at (i) );
		if ( ( (int)oper.at (i) <= 57 ) && ( (int)oper.at (i) >= 48 ) ) {
			number += oper.at (i);
		} else {
			if ( !number.empty () ) {
				number += " ";
				entry.push (number);
				number.clear ();
			}
			if ( ( symbol == "+" ) || ( symbol == "-" ) ) {
				for ( ;; ) {
					if ( entry_operators.empty () ) {
						entry_operators.push ( symbol );
						break;
					} else if ( ( entry_operators.top () == "*") ||
							( entry_operators.top () == "/" ) ) {
						entry.push ( entry_operators.top () );
						entry_operators.pop (); 
					} else {
						entry_operators.push ( symbol );
						break;
					}
				}
			} else if ( ( symbol == "*" ) || ( symbol == "/" ) ) {
				entry_operators.push ( symbol );
			} else if ( symbol == "(" ) {
				entry_operators.push ( symbol );
			} else if ( symbol == ")" ) {
				for ( ;; ) {
					if ( entry_operators.top () != "(" ) {
						entry.push ( entry_operators.top () ); 
						entry_operators.pop ();
					} else {
						entry_operators.pop();
						break;
					}
				}
			}
		}
	}
	if ( !number.empty () ) {
		number += " ";
		entry.push (number);
		number.clear ();
	}
	while ( !entry_operators.empty () ) {
		entry.push ( entry_operators.top () );
		entry_operators.pop ();
	}
	oper = "";
	while ( ! entry.empty () ) {
		oper += entry.front ();
		entry.pop ();
	}
}
string Rpn::conversion () {
	return oper;
}
void Rpn::calculate () {
	string number;
	stack<int> stack;
	for ( int i = 0; i < oper.length (); i++ ) {
		if ( ( (int)oper.at (i) <= 57 ) && ( (int)oper.at (i) >= 48 ) ) {
			number += oper.at (i);
		} else {
			if ( !number.empty () ) {
				stack.push ( stoi (number) );
				number.clear ();
			} else {
				int a, b;
				a = stack.top ();
				stack.pop ();
				b = stack.top ();
				stack.pop ();
				if ( oper.at (i) == '+' ) {
					stack.push ( b+a );
				} else if ( oper.at (i) == '-' ) {
					stack.push ( b-a );
				} else if ( oper.at (i) == '*' ) {
					stack.push ( b*a );
				} else {
					stack.push ( b/a );
				}
			}
		}
	}
	result = stack.top ();
}
int Rpn::get_result () {
	return result;
}
