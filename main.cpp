#include <iostream>
#include "Rpn.h"

using namespace std;

int main() {
	cout << "Type your operation below and click enter:\n";
	string operation;
	cin >> operation;
	Rpn conversion ( operation );
	conversion.convert ();
	//cout << conversion.conversion () << "\n";
	conversion.calculate ();
	cout << conversion.get_result () << "\n";
}
