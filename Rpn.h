#ifndef RPN_H
#define RPN_H
#include <queue>
#include <stack>
#include <iostream>

using namespace std;

class Rpn
{
	string oper = "";
	int result = 0;
	public:
		Rpn ( string );
		void convert ();
		bool is_conversion_empty ();
		string conversion ();
		void calculate ();
		int get_result ();
	protected:
};

#endif
